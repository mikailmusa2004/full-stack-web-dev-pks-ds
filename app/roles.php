<?php

namespace App;

use Stringable;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;

class roles extends Model
{
    protected $fillable = ['name'];

    protected $primarykey = 'id';

    protected $keytype = 'string';

    public $incrementing = false;

    protected static function boot(){
        parent::boot();

        static::creating(function ($model) {
            if (empty($model->{$model->getKeyName()})) {
                $model->{$model->getKeyName()} = Str::uuid();

            }
        });
    }
    
    public function users()
    {
        return $this->hasMany('App/User');
    }
}
